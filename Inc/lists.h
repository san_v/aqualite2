/*
 * lists.h
 *
 *  Created on: 25 ����. 2015
 *      Author: vorontso
 */

#ifndef INC_LISTS_H_
#define INC_LISTS_H_

#include <stdint.h>
#include "rtc.h"
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"
#include "tlv_descr.h"



typedef struct Element
{
	struct Element *next;
	RTC_TimeTypeDef time;
	uint16_t value;
} listTimeValueElementTypeDef;

typedef struct
{
	listTimeValueElementTypeDef *first;
	listTimeValueElementTypeDef *last;
	uint8_t numberOfElements;
} listTimeValueTypeDef;

typedef enum
{
	LIST_IS_EMPTY = 0,
	LIST_IS_NOT_EMPTY = 1
} listTimeValueStatusTypeDef;

void listInit(listTimeValueTypeDef *List);
void listAddElement(listTimeValueTypeDef *List, RTC_TimeTypeDef Time, uint16_t Value);
void listRemoveLastElement(listTimeValueTypeDef *List);
listTimeValueStatusTypeDef listIsEmpty(listTimeValueTypeDef *List);

#endif /* INC_LISTS_H_ */
