/*
 * freertos.h
 *
 *  Created on: 12 ����. 2015
 *      Author: San_V
 */

#ifndef INC_FREERTOS_H_
#define INC_FREERTOS_H_

#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"
#include "buffer.h"
#include "tlv.h"
#include "lists.h"

/*
 * USB WORKER task defines and external variables
 */
#define USB_RX_BUFFER_SIZE 128

//extern osMessageQId usbRxQueueHandle;
extern circularBufferHandleTypeDef usbRxBufferHandle;

//Profiles definition
extern listTimeValueTypeDef Profile[TLV_MAX_PROFILES];

extern uint32_t totalProfileElements;
extern const uint32_t ProfileBackup [TLV_MAX_PROFILES][TLV_MAX_ELEMENTS_BACKEDUP];

#endif /* INC_FREERTOS_H_ */
