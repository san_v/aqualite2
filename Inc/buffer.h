/*
 * buffer.h
 *
 *  Created on: 12 ����. 2015
 *      Author: San_V
 */

/* Work with buffers*/

#ifndef INC_BUFFER_H_
#define INC_BUFFER_H_

#include <stdint.h>

typedef struct
{
	uint8_t *buffer;
	uint16_t size;
	uint8_t readPosition;
	uint8_t writePosition;
} circularBufferHandleTypeDef;


typedef enum
{
	BUFFER_OK = 0,
	BUFFER_FULL = 1,
	BUFFER_EMPTY = 2
} bufferStatusTypeDef;

void bufferInitCircular(circularBufferHandleTypeDef *BufHandle, uint8_t *Buf, uint16_t Size);
void bufferClearCircular(circularBufferHandleTypeDef *Buf);
bufferStatusTypeDef bufferWriteCircular(circularBufferHandleTypeDef *Buf, uint8_t *Data, uint16_t Len);
bufferStatusTypeDef bufferReadCircular(circularBufferHandleTypeDef *Buf, uint8_t *Data, uint16_t Len);
bufferStatusTypeDef bufferCheckCircular(circularBufferHandleTypeDef *Buf, uint8_t *Data, uint16_t Len);
uint16_t bufferGetUsedSpaceCircular(circularBufferHandleTypeDef *Buf);
uint16_t bufferGetFreeSpaceCircular(circularBufferHandleTypeDef *Buf);

#endif /* INC_BUFFER_H_ */
