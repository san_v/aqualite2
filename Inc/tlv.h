/*
 * TLV library. Provides functions to work with Type/Length/Value buffers.
 * Works with linear Tx Buffer and Circular Rx Buffer
 * Buffers can be received/transcieved by different hardware using appropriate drivers
 * Controller: STM32F407, can be easily ported to other platform
 * Dependencies: None
 * Configure DeviceID usage in "Configuration" section below
 * Note: If Big-Endian to Little-Endian macros htons()/ntohs(), htonl()/ntohl() are already
 * defined in user code, the user needs to check whether they perform the same operation as
 * defined below in this file
 *
 * Author: Alexander Vorontsov
 * Date: 24.10.2014
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TLV_H
#define __TLV_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "tlv_descr.h"
#include "buffer.h"
#include "rtc.h"
//#include "light.h"
//#include "times.h"
#include "threads.h"
/* Exported types ------------------------------------------------------------*/

// Tx Buffer data type
typedef struct
{
	uint32_t	MaxLength;
	uint16_t	Length;
	uint8_t		*Buffer;
} TLV_TxBufferTypeDef;

// Tx Buffer function result states
typedef enum
{
  TLV_TXBUF_OK       			= 0x00,
  TLV_TXBUF_FULL		 	    = 0x01, //Not enough space in buffer. Flush the buffer and try again
  TLV_TXBUF_WRONG_LEN			= 0x02
} TLV_TxBufferStatusTypeDef;

// TLV Header type
typedef struct
{
	uint16_t Type;
	uint16_t Length;
	uint8_t *Value;
} TLV_HeaderTypeDef;

// TLV Message Handler status
typedef enum
{
	TLV_MSG_READY_FOR_PARSE	= 0,
	TLV_MSG_NONE			= 1,
	TLV_MSG_NO_DELIMITER	= 2,
	TLV_MSG_INCOMPLETE		= 3
} TLV_MessageStatusTypeDef;


/* Comment out following lines if such macros already exist in user code */
#define htons(a)            ((((a)>>8)&0xff)|(((a)<<8)&0xff00))
#define ntohs(a)            htons(a)
#define htonl(a)            ( (((a)>>24)&0xff) | (((a)>>8)&0xff00) |\
                                (((a)<<8)&0xff0000) | (((a)<<24)&0xff000000) )
#define ntohl(a)            htonl(a)

/* Exported functions ------------------------------------------------------- */
void tlvInitTxBuffer(TLV_TxBufferTypeDef *buffer);
void tlvClearTxBuffer(TLV_TxBufferTypeDef *buffer);
TLV_TxBufferStatusTypeDef tlvAppendTxBuffer(TLV_TxBufferTypeDef *buffer, uint16_t type,
																uint16_t length, uint8_t* value);

TLV_MessageStatusTypeDef tlvValidMessageInBuffer(circularBufferHandleTypeDef *Buf,
																		  uint16_t *TotalMsgLen);
void tlvParse(circularBufferHandleTypeDef *Buf, uint16_t Len);

/* Configuration -------------------------------------------------------------*/
//Comment out following line if UserID will be not used
//#define __USE_USER_ID
//Comment out following line if DeviceID will be not used
//#define __USE_DEV_ID
//Comment out following line if DeviceID will be not used
//#define __USE_TIMESTAMP

#ifdef __USE_USER_ID
//Set user ID obtained from server
#define TLV_USER_ID		0x00000001
#endif /* __USE_USER_ID */


#ifdef __USE_DEV_ID
//Set device ID obtained from server
#define TLV_DEV_ID		103
#endif /* __USE_DEV_ID */

// Configuration
#define TLV_MSG_DELIMITER		0xABD5


//Constants
#define TLV_HDR_LEN				4 //Type+Length = 2+2 = 4Bytes

#ifndef NULL
#define NULL ((void *)0)
#endif /*NULL*/

#endif /* __TLV_H */
