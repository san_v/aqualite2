/*
 * tlv_descr.h
 *
 *  Created on: 28 ��. 2015
 *      Author: vorontso
 */

#ifndef USERCODE_INCLUDE_TLV_DESCR_H_
#define USERCODE_INCLUDE_TLV_DESCR_H_


// Simple TLVs
#define	TLV_TIME		0x0020	//Time in seconds from 00:00
#define TLV_VALUE_TIME	0x0030  //[uint8_t value, uint24_t time]*N

//Command TLVs
#define TLV_GET_PROFILE		0x0100 // Get (request) profile#N (N=Value) data
#define TLV_PUT_PROFILE		0x0101 // Put (response) profile#N (N=Value) data
#define TLV_TEST_PROFILE	0x0102 // Run profile#N (N=Value) test
#define TLV_WRITE_FLASH		0x0103 // Backup profile#N (N=Value) to flash memory, 0xffff means backup all



//Profiles
#define TLV_MAX_PROFILES				8
#define TLV_MAX_ELEMENTS_IN_PROFILE		32
#define TLV_MAX_ELEMENTS_BACKEDUP		12
#define TLV_PROFILE_DATETIME	0x0	// Time, Timezone, DaylighSaving, Date, TimestampUnix, TimestampNTP

#define TLV_PROFILE_0_CHANNEL			TIM_CHANNEL_1
#define TLV_PROFILE_1_CHANNEL			TIM_CHANNEL_2
#define TLV_PROFILE_2_CHANNEL			TIM_CHANNEL_3
#define TLV_PROFILE_3_CHANNEL			TIM_CHANNEL_4
#define TLV_PROFILE_4_CHANNEL			TIM_CHANNEL_1
#define TLV_PROFILE_5_CHANNEL			TIM_CHANNEL_2
#define TLV_PROFILE_6_CHANNEL			TIM_CHANNEL_3
#define TLV_PROFILE_7_CHANNEL			TIM_CHANNEL_4
#define TLV_PROFILE_CHANNEL(x)			TLV_PROFILE_##x##_CHANNEL





#endif /* USERCODE_INCLUDE_TLV_DESCR_H_ */
