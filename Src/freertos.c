/**
  ******************************************************************************
  * File Name          : freertos.c
  * Date               : 11/06/2015 15:51:37
  * Description        : Code for freertos applications
  ******************************************************************************
  *
  * COPYRIGHT(c) 2015 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */
#include "threads.h"
//#include "buffer.h"
#include "usbd_cdc_if.h"
#include "tim.h"
#include "rtc.h"
#include "usb_device.h"
#include "lists.h"
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId defaultTaskHandle;
osThreadId usbWorkerHandle;
//osMessageQId usbRxQueueHandle;

/* USER CODE BEGIN Variables */
circularBufferHandleTypeDef usbRxBufferHandle;
uint8_t usbRxBuffer[USB_RX_BUFFER_SIZE];

listTimeValueTypeDef Profile[TLV_MAX_PROFILES];

uint32_t totalProfileElements;
__attribute__((__section__(".eeprom"))) const uint32_t ProfileBackup [TLV_MAX_PROFILES][TLV_MAX_ELEMENTS_BACKEDUP] = {
											{0x00000011,0x010000ff,0x0500ab00,0xffffffff},
											{0xffffffff},
											{0xffffffff},
											{0xffffffff},
											{0xffffffff},
											{0xffffffff},
											{0xffffffff},
											{0xffffffff}
										};

typedef struct
{
	TIM_HandleTypeDef *htim;
	uint32_t		  channel;
} profileToPwmChannelMappingTypeDef;

profileToPwmChannelMappingTypeDef pwmChannelMap [TLV_MAX_PROFILES] =
		{
				{.channel = TLV_PROFILE_CHANNEL(0)},
				{.channel = TLV_PROFILE_CHANNEL(1)},
				{.channel = TLV_PROFILE_CHANNEL(2)},
				{.channel = TLV_PROFILE_CHANNEL(3)},
				{.channel = TLV_PROFILE_CHANNEL(4)},
				{.channel = TLV_PROFILE_CHANNEL(5)},
				{.channel = TLV_PROFILE_CHANNEL(6)},
				{.channel = TLV_PROFILE_CHANNEL(7)}
		};

/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void StartDefaultTask(void const * argument);
void StartTaskUsbWorker(void const * argument);

extern void MX_USB_DEVICE_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */
RTC_TimeTypeDef uintToTime(uint32_t timestamp);
uint32_t timeToUint(RTC_TimeTypeDef time);
uint16_t pwmGetPulseFromProfile(listTimeValueTypeDef *List, uint32_t timestamp);
/* USER CODE END FunctionPrototypes */
/* Hook prototypes */

/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
       
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of usbWorker */
/*  osThreadDef(usbWorker, StartTaskUsbWorker, osPriorityHigh, 0, 128);
  usbWorkerHandle = osThreadCreate(osThread(usbWorker), NULL);*/

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Create the queue(s) */
  /* definition and creation of usbRxQueue */
/*  osMessageQDef(usbRxQueue, 16, uint8_t);
  usbRxQueueHandle = osMessageCreate(osMessageQ(usbRxQueue), NULL);
*/
  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{
  /* init code for USB_DEVICE */
//  MX_USB_DEVICE_Init();

  /* USER CODE BEGIN StartDefaultTask */
	//Start timers
	  HAL_TIM_Base_Start(&htim3);
	  HAL_TIM_Base_Start(&htim4);

	//Read initial configuration from profiles FLASH and start PWM
	{
	  uint32_t i,j;
	  for (i=0;i<TLV_MAX_PROFILES;i++)
	  {
		  listInit(&Profile[i]);

		  //Finalize channel to profile mapping initialization
		  if (i<=3) pwmChannelMap[i].htim = &htim3;
		  else pwmChannelMap[i].htim = &htim4;

		  //Start PWM
		  HAL_TIM_PWM_Start(pwmChannelMap[i].htim,pwmChannelMap[i].channel);
		  for (j=0;j<32;j++)
		  {
			  if (ProfileBackup[i][j] != 0xffffffff)
			  {
				  RTC_TimeTypeDef time;
				  uint16_t value;
				  uint32_t tmp = ProfileBackup[i][j]&0x00ffffff;
				  value = (uint16_t)(ProfileBackup[i][j]>>24)*10;
				  time = uintToTime(tmp);
				  listAddElement(&Profile[i],time,value);
				  totalProfileElements++;
			  } else break;
		  }
	  }
	}



	  osThreadDef(usbWorker, StartTaskUsbWorker, osPriorityNormal, 0, 128);
	  usbWorkerHandle = osThreadCreate(osThread(usbWorker), NULL);

  /* Infinite loop */
  for(;;)
  {
	  uint32_t i;
	  for (i=0;i<TLV_MAX_PROFILES;i++)
	  {
		  RTC_TimeTypeDef time;
		  HAL_RTC_GetTime(&hrtc,&time,RTC_FORMAT_BIN);
		  uint16_t pwmPulse = pwmGetPulseFromProfile(&Profile[i],timeToUint(time));

		  __IO uint32_t *currentCCR;
		  currentCCR = &(pwmChannelMap[i].htim->Instance->CCR1) + i%4;
		  *currentCCR = pwmPulse;
	  }
	  osThreadYield();
  }
  /* USER CODE END StartDefaultTask */
}

/* StartTaskUsbWorker function */
void StartTaskUsbWorker(void const * argument)
{
  /* USER CODE BEGIN StartTaskUsbWorker */
	/*Init Rx Buffer */
	bufferInitCircular(&usbRxBufferHandle,usbRxBuffer,USB_RX_BUFFER_SIZE);

	uint16_t currentMessageLen = 0;

  /* Infinite loop */
  for(;;)
  {
	  if (hUsbDeviceFS.dev_state == USBD_STATE_CONFIGURED)  //Check usb connection
	  {
		  volatile static TLV_MessageStatusTypeDef status;
		  if(currentMessageLen == 0)
		  {
			  status = tlvValidMessageInBuffer(&usbRxBufferHandle,&currentMessageLen);
		  }
		  if (currentMessageLen > 0)
		  {
			  /*
			   * Parse message
			   */
			  if (bufferGetUsedSpaceCircular(&usbRxBufferHandle) >= currentMessageLen)
			  {
				  tlvParse(&usbRxBufferHandle,currentMessageLen);
				  currentMessageLen = 0;
			  }
		  }
		  RTC_TimeTypeDef Time;
		  HAL_RTC_GetTime(&hrtc,&Time,RTC_FORMAT_BIN);
		  CDC_Transmit_FS((uint8_t*)&Time,3);
	  }


	osDelay(1010);
//    osThreadYield();
  }
  /* USER CODE END StartTaskUsbWorker */
}

/* USER CODE BEGIN Application */
RTC_TimeTypeDef uintToTime(uint32_t timestamp)
{
	RTC_TimeTypeDef time;
	if (timestamp < 86400)
	{
	time.Hours = timestamp/3600;
	time.Minutes = (timestamp%3600)/60;
	time.Seconds = (timestamp%3600)%60;
	}
	else
	{
		time.Hours = 0;
		time.Minutes = 0;
		time.Seconds = 0;
	}
	return time;
}

uint32_t timeToUint(RTC_TimeTypeDef time)
{
	uint32_t result;
	result = time.Hours*3600 + time.Minutes*60 + time.Seconds;
	return result;
}

uint16_t pwmGetPulseFromProfile(listTimeValueTypeDef *List, uint32_t timestamp)
{
	uint16_t result;
	uint32_t i;
	if (listIsEmpty(List) == LIST_IS_EMPTY)
	{
		result = 0;
	}
	else
	{
		//Check whether current timestamp is between Last and First entry (crossing 00:00)
		if ((timestamp < timeToUint(List->first->time))||(timestamp >= timeToUint(List->last->time)))
		{
			result = List->last->value;
		}
		else
		{
			listTimeValueElementTypeDef *tmp = List->first;
			while (tmp != List->last)
			{
				if ((timestamp >= timeToUint(tmp->time))&&(timestamp < timeToUint(tmp->next->time)))
				{
					result = tmp->value;
					break;
				}
				else
				{
					tmp = tmp->next;
				}
			}
		}
	}
	return result;
}

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
