/*
 * buffer.c
 *
 *  Created on: 12 ����. 2015
 *      Author: San_V
 */

/*Functions to work with buffers */

#include "buffer.h"


/*Circular buffers*/

void bufferInitCircular(circularBufferHandleTypeDef *BufHandle, uint8_t *Buf, uint16_t Size)
{
	BufHandle->buffer = Buf;
	BufHandle->size = Size;
	bufferClearCircular(BufHandle);
}

void bufferClearCircular(circularBufferHandleTypeDef *Buf)
{
	Buf->readPosition = 0;
	Buf->writePosition = 0;
}

uint16_t bufferGetUsedSpaceCircular(circularBufferHandleTypeDef *Buf)
{
	uint16_t result;
	if (Buf->writePosition >= Buf->readPosition)
	{
		result = Buf->writePosition - Buf->readPosition;
	}
	else
	{
		result = Buf->size - Buf->readPosition + Buf->writePosition;
	}
	return result;
}

uint16_t bufferGetFreeSpaceCircular(circularBufferHandleTypeDef *Buf)
{
	uint16_t result;
	result = Buf->size - bufferGetUsedSpaceCircular(Buf);
	return result;
}

bufferStatusTypeDef bufferWriteCircular(circularBufferHandleTypeDef *Buf, uint8_t *Data, uint16_t Len)
{
	uint16_t freeSpace = bufferGetFreeSpaceCircular(Buf);
	if ((freeSpace <= Len))
		{
			return BUFFER_FULL;
		}
	else
	{
		uint16_t i;
		uint8_t *ptr_buffer, *ptr_data;
		ptr_buffer = Buf->buffer + Buf->writePosition;
		ptr_data = Data;
		for(i=0;i<Len;i++)
		{
			*ptr_buffer = *ptr_data;
			ptr_data++;
			Buf->writePosition++;
			if (Buf->writePosition >= Buf->size)
				{
					Buf->writePosition = 0;
					ptr_buffer = Buf->buffer;
				}
			else ptr_buffer++;
		}
		return BUFFER_OK;
	}
}

bufferStatusTypeDef bufferReadCircular(circularBufferHandleTypeDef *Buf, uint8_t *Data, uint16_t Len)
{
	if (bufferGetUsedSpaceCircular(Buf) < Len)
	{
		return BUFFER_EMPTY;
	}
	else
	{
		uint16_t i;
		uint8_t *ptr_buffer, *ptr_data;
		ptr_buffer = Buf->buffer + Buf->readPosition;
		ptr_data = Data;
		for(i=0;i<Len;i++)
		{
			*ptr_data = *ptr_buffer;
			ptr_data++;
			Buf->readPosition++;
			if (Buf->readPosition >= Buf->size)
				{
					Buf->readPosition = 0;
					ptr_buffer = Buf->buffer;
				}
			else ptr_buffer++;
		}
		return BUFFER_OK;
	}
}


bufferStatusTypeDef bufferCheckCircular(circularBufferHandleTypeDef *Buf, uint8_t *Data, uint16_t Len)
{
	if (bufferGetUsedSpaceCircular(Buf) < Len)
	{
		return BUFFER_EMPTY;
	}
	else
	{
		uint16_t i;
		uint8_t *ptr_buffer, *ptr_data;
		ptr_buffer = Buf->buffer + Buf->readPosition;
		ptr_data = Data;
		for(i=0;i<Len;i++)
		{
			*ptr_data = *ptr_buffer;
			ptr_data++;
//			Buf->readPosition++;
			if ( (ptr_data - Data) >= Buf->size)
				{
					ptr_buffer = Buf->buffer;
				}
			else ptr_buffer++;
		}
		return BUFFER_OK;
	}
}
