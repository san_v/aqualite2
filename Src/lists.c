/*
 * lists.c
 *
 *  Created on: 25 ����. 2015
 *      Author: vorontso
 */

#include "lists.h"


listTimeValueTypeDef Profile[TLV_MAX_PROFILES];


void listInit(listTimeValueTypeDef *List)
{
	List->first = NULL;
	List->last = NULL;
	List->numberOfElements = 0;
}


void listAddElement(listTimeValueTypeDef *List, RTC_TimeTypeDef Time, uint16_t Value)
{
	listTimeValueElementTypeDef *tmp = (listTimeValueElementTypeDef*)pvPortMalloc(sizeof(listTimeValueElementTypeDef));

	//Check that memory allocated correctly
	if (tmp != NULL)
	{
	    tmp->time = Time;
	    tmp->value = Value;

	    if (List->numberOfElements == 0)
	    {
	    	List->first = tmp;
	    }
	    else
	    {
	    	List->last->next = tmp;
	    }
	    List->last = tmp;
	    tmp->next = List->first;
	    List->numberOfElements++;
	}
}

void listRemoveLastElement(listTimeValueTypeDef *List)
{
	if (List->numberOfElements >0)
	{
		listTimeValueElementTypeDef *tmpElement = List->first;
		while (tmpElement->next != List->last)
		{
			tmpElement = tmpElement->next;
		}

		vPortFree((void*)List->last);
		{
		if (List->numberOfElements >1)
		{
			tmpElement->next = List->first;
			List->numberOfElements--;
			List->last = tmpElement;
		}
		else listInit(List);
		}
	}
}

listTimeValueStatusTypeDef listIsEmpty(listTimeValueTypeDef *List)
{
	if ((List->first==NULL)&&(List->last==NULL)&&(List->numberOfElements==0))
	{
		return LIST_IS_EMPTY;
	}
	else
	{
		return LIST_IS_NOT_EMPTY;
	}
}
