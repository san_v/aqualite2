/*
 * TLV library. Provides functions to work with Type/Length/Value buffers.
 * Works with linear Tx Buffer and Circular Rx Buffer
 * Buffers can be received/transcieved by different hardware using appropriate drivers
 * Controller: Platform-independent, designed for Little-Endian CPUs
 * Dependencies: None
 *
 * Author: Alexander Vorontsov
 * Date: 24.10.2014
 */

/* Includes ------------------------------------------------------------------*/
#include "tlv.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Exported variables ---------------------------------------------------------*/
extern RTC_TimeTypeDef uintToTime(uint32_t timestamp);
extern uint32_t timeToUint(RTC_TimeTypeDef time);
/* Private function prototypes -----------------------------------------------*/
static void tlvReadHeader(circularBufferHandleTypeDef *Buf, uint16_t *Type, uint16_t *Length);
static void tlvReadUint16(circularBufferHandleTypeDef *Buf, uint16_t *Value);
static void tlvReadUint32(circularBufferHandleTypeDef *Buf, uint32_t *Value);
static void tlvReadTime(circularBufferHandleTypeDef *Buf, uint32_t *Time);
/* Private functions ---------------------------------------------------------*/

/*
 * Init Tx Buffer
 */
void tlvInitTxBuffer(TLV_TxBufferTypeDef *buffer)
{
	uint16_t *ptr = (uint16_t *) buffer->Buffer;
	//Set delimiter
	*ptr = htons(0xABD5);
	tlvClearTxBuffer(buffer);
}

/*
 * Clear Tx Buffer (alias for Init Tx Buffer)
 */
void tlvClearTxBuffer(TLV_TxBufferTypeDef *buffer)
{
		//Clear Length
		buffer->Length = 0;
		uint16_t *ptr = (uint16_t *) buffer->Buffer + 1;
		*ptr = 0x00;

//Append user ID
#ifdef __USE_USER_ID
		uint32_t tmp = htonl(TLV_USER_ID);
		tlvAppendTxBuffer(buffer,0xFFF0,4,(uint8_t *) &tmp);
#endif /* __USE_USER_ID */

//Append device ID
#ifdef __USE_DEV_ID
		uint16_t tmp1 = htons(TLV_DEV_ID);
		tlvAppendTxBuffer(buffer,0xFFF1,2,(uint8_t *) &tmp1);

#endif /* __USE_DEV_ID */

//Append Timestamp
#ifdef __USE_TIMESTAMP
		uint32_t tmp2 = timesGetCurrentNixTime();
		tlvAppendTxBuffer(buffer,0xFFF2,4,(uint8_t *) &tmp2);
#endif /*__USE_TIMESTAMP*/
}

TLV_TxBufferStatusTypeDef tlvAppendTxBuffer(TLV_TxBufferTypeDef *buffer, uint16_t type,
																uint16_t length, uint8_t* value)
{
	if ((buffer->MaxLength < (length + 4))||(length == 0)) {return TLV_TXBUF_WRONG_LEN;}
	if ((buffer->MaxLength - buffer->Length) >= (length+4))
	{
		uint8_t *ptr = buffer->Buffer + 4 + buffer->Length;
		uint16_t *ptr1 = (uint16_t *) ptr;
		//Append Type
		*ptr1 = htons(type);
		ptr1++;
		//Append Length
		*ptr1 = htons(length);
		ptr1++;
		ptr = (uint8_t *) ptr1;
		uint32_t i;
			for (i=0;i<length;i++)
			{
				*ptr = *value;
				ptr++;
				value++;
			}
			//Change TML
			buffer->Length += length + 4;
			ptr1 = (uint16_t *) buffer->Buffer + 1;
			*ptr1 = htons(buffer->Length);
	return TLV_TXBUF_OK;
	}
	else {
		return TLV_TXBUF_FULL;
	}
}

TLV_MessageStatusTypeDef tlvValidMessageInBuffer(circularBufferHandleTypeDef *Buf, uint16_t *TotalMsgLen)
{
	if (bufferGetUsedSpaceCircular(Buf)<9) return TLV_MSG_NONE; //Too short data in buffer -> no messages
	else
	{
		uint16_t tmp;
		bufferCheckCircular(Buf,(uint8_t*)&tmp,2);
		if (ntohs(tmp) != TLV_MSG_DELIMITER)
			{
				bufferReadCircular(Buf,(uint8_t*)&tmp,1); // Flush one byte
				return TLV_MSG_NO_DELIMITER;
			}
		else
		{
			bufferReadCircular(Buf,(uint8_t*)&tmp,2); //Flush delimiter
			bufferReadCircular(Buf,(uint8_t*)&tmp,2); //Read total message length
			*TotalMsgLen = ntohs(tmp);
			if (bufferGetUsedSpaceCircular(Buf) < *TotalMsgLen)
			{
				return TLV_MSG_INCOMPLETE;
			}
		}
	}
	return TLV_MSG_READY_FOR_PARSE;
}


static void tlvReadHeader(circularBufferHandleTypeDef *Buf, uint16_t *Type, uint16_t *Length)
{
	bufferReadCircular(Buf,(uint8_t*)Type,2); //Read 2-byte Type value
	*Type = ntohs(*Type);
	bufferReadCircular(Buf,(uint8_t*)Length,2); //Read 2-byte Length value
	*Length = ntohs(*Length);
}

static void tlvReadTime(circularBufferHandleTypeDef *Buf, uint32_t *Time)
{
	tlvReadUint32(Buf,Time);
}

static void tlvReadUint16(circularBufferHandleTypeDef *Buf, uint16_t *Value)
{
	bufferReadCircular(Buf,(uint8_t*)Value,2); //Read 2-byte value
	*Value = ntohs(*Value);
}

static void tlvReadUint32(circularBufferHandleTypeDef *Buf, uint32_t *Value)
{
	bufferReadCircular(Buf,(uint8_t*)Value,4); //Read 4-byte value
	*Value = ntohl(*Value);
}


void tlvParse(circularBufferHandleTypeDef *Buf, uint16_t Len)
{
	uint16_t ByteCount = Len;
	uint16_t Type,Length;
//	uin32_t currentProfile, currentCommand;
	while (ByteCount > 0)
	{
/*		bufferReadCircular(Buf,(uint8_t*)&Type,2); //Read 2-byte Type value
		Type = ntohs(Type);
		bufferReadCircular(Buf,(uint8_t*)&Length,2); //Read 2-byte Length value
		Length = ntohs(Length);*/
		tlvReadHeader(Buf,&Type,&Length);
		ByteCount -= TLV_HDR_LEN;

		switch (Type) {

			case (TLV_PUT_PROFILE) :
			{
				uint16_t profile;
/*				uint32_t Time;
				uint16_t Value;*/
				tlvReadUint16(Buf,&profile);
				ByteCount -= sizeof(profile);

				if (ByteCount >= 8)
				{
					tlvReadHeader(Buf,&Type,&Length);
					ByteCount -= TLV_HDR_LEN;

					if (Type == TLV_VALUE_TIME)
					{
						if (Length%6 == 0)
						{
							uint32_t i;
							for (i=0;i<(Length/6);i++)
							{
								  RTC_TimeTypeDef time;
								  uint16_t value;
								  uint32_t tmp;
								  tlvReadUint16(Buf,&value);
								  ByteCount -= sizeof(value);
								  tlvReadUint32(Buf,&tmp);
								  time = uintToTime(tmp);

								  if (i < Profile[profile].numberOfElements)
								  {
									uint32_t j;
									listTimeValueElementTypeDef *tmpElement = Profile[profile].first;
									for (j=0;j<i;j++)
									{
										tmpElement = tmpElement->next;
									}
									tmpElement->time = time;
									tmpElement->value = value;
								  }
								  else
								  {
									  if (totalProfileElements < TLV_MAX_PROFILES*TLV_MAX_ELEMENTS_IN_PROFILE)
									  {
									  listAddElement(&Profile[profile],time,value);
									  totalProfileElements++;
									  }
								  }
							}
							if (i<Profile[profile].numberOfElements)
							{
								while(Profile[profile].numberOfElements > i)
								{
									listRemoveLastElement(&Profile[profile]);
									totalProfileElements--;
								}
							}
						}
					}
					else
					{
						//Flush the rest of malformed packet
						uint8_t tmp;
						while (ByteCount > 0)
							{
								bufferReadCircular(Buf,&tmp,1);
								ByteCount--;
							}
					}
				}
			}
				break;

			case (TLV_WRITE_FLASH) :
			{
				uint16_t profile;
				tlvReadUint16(Buf,&profile);
				if (profile == 0xffff)
				{
					osThreadSuspendAll();
					HAL_FLASH_Unlock();
					FLASH_EraseInitTypeDef flashHandle;
					flashHandle.PageAddress = 0x8000400;
					flashHandle.TypeErase = FLASH_TYPEERASE_PAGES;
					flashHandle.NbPages = 1;
					flashHandle.Banks = FLASH_BANK_1;
					uint32_t pageError;
					HAL_FLASHEx_Erase(&flashHandle,&pageError);
					if (pageError == 0xffffffff)
					{
						uint32_t i,j;
						for (i=0;i<TLV_MAX_PROFILES;i++)
						{
							if (listIsEmpty(&Profile[i]) == LIST_IS_NOT_EMPTY)
						 {
							listTimeValueElementTypeDef *tmp = Profile[i].first;
							for (j=0;j<TLV_MAX_ELEMENTS_BACKEDUP;j++)
							{

								uint32_t backupValue;
								backupValue = (((tmp->value)/10)<<24) + timeToUint(tmp->time);
								HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,(uint32_t)&ProfileBackup[i][j],backupValue);
								tmp = tmp->next;
								if (tmp == Profile[i].first)
								{
//									HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,&ProfileBackup[i][j],0xffffffff);
									break;
								}
							}
						 }
						}

					}
					HAL_FLASH_Unlock();

					osThreadResumeAll();
				}
				else
				{
					/*
					 * Parse profile numbers here
					 * Probably will be not needed in current realization due
					 * only single 1K block allocated for all backup data
					 */
				}
			}
			break;

			case (TLV_TIME) :
			{
				if (Length == 0)
				{
					//Process Time request here
				}
				else
				{
				uint32_t Time;
/*				bufferReadCircular(Buf,(uint8_t*)&Time,4); //Read 4-byte Time value
				Time = ntohl(Time);*/
				tlvReadTime(Buf,&Time);
				ByteCount -= sizeof(Time);

				RTC_TimeTypeDef rtcTime;
				rtcTime.Hours = Time/(3600);
				rtcTime.Minutes = (Time-rtcTime.Hours*3600)/60;
				rtcTime.Seconds = Time-(rtcTime.Hours*3600 + rtcTime.Minutes*60);
				HAL_RTC_SetTime(&hrtc,&rtcTime,RTC_FORMAT_BIN);
				}
			}
				break;



			default:
			{
				 ByteCount = 0;
			}
				break;
		}

	}
}
